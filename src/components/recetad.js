import React from "react"
import { Link } from "gatsby"

import Bio from "./bio"
import Layout from "./layout"
import SEO from "./seo"

class RecetaTemplate extends React.Component {

  render() {
    const post = this.props.data.markdownRemark
    const siteTitle = this.props.data.site.siteMetadata.title
    const { previous, next } = this.props.pageContext
    const prereceta = post.frontmatter.pre_receta
    const receta = post.frontmatter.receta

    function showReceta(receta) {
      if (receta !== undefined && receta.length > 0) {
        return (
          <table>
            <thead>
            <tr>
              <th>Ingrediente</th>
              <th>Cantidad</th>
              <th>Porcentaje</th>
              <th>Comentario</th>
            </tr>
            </thead>
            <tbody>
            {receta.map(({ Ingrediente, Cantidad, Porcentaje, Comentario }) => {
              return (
                <tr>
                  <td>{Ingrediente}</td>
                  <td>{Cantidad}</td>
                  <td>{Porcentaje}</td>
                  <td>{Comentario}</td>
                </tr>
              )
            })}
            </tbody>
          </table>
        )
      }
    }

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO
          title={post.frontmatter.title}
          description={post.frontmatter.description || post.excerpt}
        />
        <article>
          <header>
            <h1 className="text-5xl font-black mt-8 mb-0">
              {post.frontmatter.title}
            </h1>
            <p className="text-sm leading-loose mb-8 ">
              {post.frontmatter.date}
            </p>
          </header>

          <section>
            <h2 className="text-2xl font-black mt-8 mb-0">Masa Madre</h2>
            {showReceta(prereceta)}
          </section>

          <section>
            <h2 className="text-2xl font-black mt-8 mb-0">Masa final</h2>
            {showReceta(receta)}
          </section>
          <section
            className="markdown"
            dangerouslySetInnerHTML={{ __html: post.html }}
          />
          <hr className="h-px mb-8"/>
          <footer>
            <Bio/>
          </footer>
        </article>

        <nav>
          <ul
            className="flex flex-wrap justify-between mb-8"
            // style={{
            //   display: `flex`,
            //   flexWrap: `wrap`,
            //   justifyContent: `space-between`,
            //   listStyle: `none`,
            //   padding: 0,
            // }}
          >
            <li>
              {previous && (
                <Link
                  className="text-blue-600"
                  to={previous.fields.slug}
                  rel="prev"
                >
                  ← {previous.frontmatter.title}
                </Link>
              )}
            </li>
            <li>
              {next && (
                <Link
                  className="text-blue-600"
                  to={next.fields.slug}
                  rel="next"
                >
                  {next.frontmatter.title} →
                </Link>
              )}
            </li>
          </ul>
        </nav>
      </Layout>
    )
  }
}

export default RecetaTemplate

/*
export const pageQuery = graphql`
  query RecetaBySlug($slug: String!) {
    site {
      siteMetadata {
        title
      }
    }
    markdownRemark(fields: { slug: { eq: $slug } }) {
      id
      excerpt(pruneLength: 160)
      html
      frontmatter {
        date
        fuente
        posttype
        tipo
        title
        pre_receta {
          Ingrediente
          Cantidad
          Porcentaje
          Comentario
        }
        receta {
          Cantidad
          Comentario
          Ingrediente
          Porcentaje
        }
      }
    }
  }
`
*/
