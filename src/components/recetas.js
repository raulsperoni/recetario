import React from "react"
import { Link } from "gatsby"

import Bio from "./bio"
import Layout from "./layout"
import SEO from "./seo"

class Recetas extends React.Component {
  render() {
    const { data } = this.props
    console.log(this.props)
    const siteTitle = data.site.siteMetadata.title
    const recetas = data.recetario.edges

    return (
      <Layout location={this.props.location} title={siteTitle}>
        <SEO title="Recetas"/>
        <Bio/>
        {recetas.map(({ node }) => {
          const title = node.frontmatter.title || node.fields.slug
          return (
            <article key={node.fields.slug}>
              <header>
                <h3 className="text-2xl font-black mt-16 mb-2">
                  <Link
                    className="text-blue-600 shadow-none"
                    to={`/recetas${node.fields.slug}`}
                  >
                    {title}
                  </Link>
                </h3>
                <small>{node.frontmatter.date}</small>
              </header>
              <section>
                <p
                  className="mb-8"
                  dangerouslySetInnerHTML={{
                    __html: node.frontmatter.description || node.excerpt,
                  }}
                />
              </section>
            </article>
          )
        })}
      </Layout>
    )
  }
}

export default Recetas

/*
export const recetasPageQuery = graphql`
  query {
    site {
      siteMetadata {
        title
      }
    }
    recetario: allMarkdownRemark(sort: { fields: [frontmatter___date], order: DESC } filter: {fileAbsolutePath: {regex: "/recetario/.*\\\\.md$/"}}) {
      edges {
        node {
          id
          fields {
            slug
          }
          frontmatter {
            date
            fuente
            posttype
            tipo
            title
            pre_receta {
              Ingrediente
              Cantidad
              Porcentaje
              Comentario
            }
            receta {
              Cantidad
              Comentario
              Ingrediente
              Porcentaje
            }
          }
        }
      }
    }
  }
`
*/
