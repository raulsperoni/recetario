---
title: "Pizza de Masa Madre"
date: "2020-08-11T10:00:00"
temperatura: 15
receta: pizza-mm
foto: IMG_20200808_215859.jpg

---

La Pizza es todo un tema eh! ¿Se supone que es lo más fácil no? Pero yo todavía le busco la vuelta. Busco bordes hinchados y tostaditos, piso firme y fino. No es changa. Dice Ken en el libro que estoy leyendo, que cualquier masa de pan puede servir para pizza y focaccia, pero esta vez seguí una receta del pelado de Gluten Morgen. Es fácil de hacer, va a la heladera en bollitos y quedan prontas en un rato de horno.

La receta y el itinerario al final. 👇

### Viernes

Mientras hacía el [pan con centeno](https://raulsperoni.gitlab.io/recetario/pan-centeno-sabado/) preparé también una masa de pizza aprovechando la masa madre que alimenté de mañana. Después de mezclar todos los ingredientes al mismo tiempo, se deja reposar unas horas la masa, luego se divide y a la heladera. En mi caso dividí en cuatro bollitos y los puse en tuppers individuales.

### Sábado

El sábado de noche decidí poner una de las pizzas al horno, la saqué de la heladera, la estiré un poco con la mano cuidando mantener el borde y la metí bien abajo en el horno a 250 grados. Estuvo rica, pero no genial. (no importa porque tenía un pan espectacular) No creció demasiado, faltó fermentar. Si se fijan en la receta lleva poca masa madre en comparación con los panes, y menos agua, así que es lógico que demore más en fermentar.

### Domingo

El domingo ya los bollitos habían crecido un montón dentro de la heladera, fueron al horno y pum. Crecieron como debían, bordes inflados, tostados y el sabor espectacular. ¿El piso? meh. Quizá mucha salsa, o tengo que hacer algo distinto con el horno pero no fue el piso que quería. Si hubiera puesto más aceite en la asadera habría funcionado mejor, lo he hecho, pero queda como ese gusto a frito que no tiene nada que ver. ¿Tendré que comprar una piedra de esas?

### Martes

Quedaba un bollito en la heladera, super fermentado. Lo hice por miedo a que se pusiera feo pero no tenía mal aspecto. Decidí porque sí nomás intentar hacerlo en la olla de hierro sobre la hornalla. ¿El piso? Gran piso gran. La dejé un ratito sin salsa y la puse después (me quemé un dedo obvio). Creció mucho más que las otras, más burbujas, más hinchada y aireada. Pero hace falta compensar de alguna manera el calor de abajo con el de arriba, esta vez fue sin muzzarela (pero con huevos revueltos, ja!) pero si hubiera tenido no se habría fundido bien.

### Conclusión

La masa queda espectacular, aguanta hasta cuatro días en la heladera y Raúl no sabe usar el horno. Seguiremos experimentando.

### Receta
